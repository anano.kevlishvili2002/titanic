import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    df['Title'] = df['Name'].str.extract(r',\s(.*?)[.]')
    titles_to_consider = ["Mr.", "Mrs.", "Miss."]
    results = []
    for title in titles_to_consider:
        median_age = df.loc[df['Title'] == title, 'Age'].median()
        median_age = round(median_age)  # Round the median to the nearest integer
        missing_values = df.loc[(df['Title'] == title) & df['Age'].isnull()].shape[0]
        results.append((title, missing_values, median_age))
    return results
